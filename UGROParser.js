var XLSX = require('xlsx');
var workbook = XLSX.readFile('reference reverse file.xlsx', {
    type: 'binary',
    cellDates: true,
    cellNF: false,
    cellText: false
});

var sheet_name_list = workbook.SheetNames;
var worksheet = workbook.Sheets[0];
var s1 = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);
var s2 = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[1]]);
var list_array = [];
for (var element of s1) {
    item = {};
    for (var obj of s2) {
        column_heading = obj['Input Column Name'];
        op_key = obj['Output Key'];
        k = element[column_heading];
        if (element['Validations']) {
            for (var obj of s1) {
                k = obj[column_heading];
                if (obj[column_heading] === 'Loan Amount') {
                    k = myFunc1(obj['Document Value'], obj[column_heading]);
                } else if (obj[column_heading] === 'Interest Rate') {
                    k = myFunc2(obj[column_heading]);
                } else if (obj[column_heading] === 'Net Payment to Customer') {
                    k = myFunc3(obj['Loan Amount'], obj['Interest Amount'], obj['PF&GST']);
                } else if (obj[column_heading] === 'Expiry date') {
                    k = myFunc4(obj['Date'], obj[column_heading]);
                }
            }
        }
        if (column_heading === 'Date' || column_heading === 'Expiry date') {
            k = JSON.stringify(k)
            k = k.split('T')[0];
        }
        if (op_key)
            item[op_key] = k;
    }
    list_array.push(item);
}
console.log(list_array);

function myFunc1(obj1, obj2) {
    if (obj2 <= obj1) {
        return obj2;
    } else {
        return 'Invalid Data';
    }
}

function myFunc2(obj) {
    if (obj <= 100 && obj >= 0) {
        return obj;
    } else {
        return 'Invalid Data';
    }
}

function myFunc3(a, b, c) {
    return (a - b - c);
}

function myFunc4(a, b) {
    if (b > a) {
        return b;
    } else {
        return 'Invalid Data';
    }
}